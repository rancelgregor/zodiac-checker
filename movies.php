<!DOCTYPE html>
<html>
<head>
	<title>Movies</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/minty/bootstrap.css">
</head>
<body>
	<h1 class="text-center">Movies</h1>
	<?php
		$movies = [["title" => "Titanic", "year" => 1997, "Director" => "James Cameron", "genre" =>"Disaster"],["title" => "Catch Me If You Can", "year"=> 2001, "Director" => "Rancel Po", "genre" => "Comedy"], ["title" => "American Pie", "year" => 2000, "Director" => "Yhong Dee", "genre" => "Horror"]];

	?>
	<div class="container">
	<div class="row">
		<?php
			foreach($movies as $movie){
		?>
<!-- 					// echo "<h1>$movieKey is $movieDetail <br></h1>";
					// echo "Movie Title: " . $movie['title'] . "</br>"; -->
		
	<div class="card mx-2 my-3 w-25">
		<h1>Movie Title: <?php echo $movie["title"] ?></h1>
		<p>Director: <?php echo $movie["Director"] ?></p>
		<p>Year: <?php echo $movie["year"] ?></p>
		<p>Genre: <?php echo $movie["genre"] ?></p>

	</div>
	<?php
		
			}
		?>
	</div>
	</div>
</body>
</html>