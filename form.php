<!DOCTYPE html>
<html>
<head>
	<title>Zodiac Checker</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/minty/bootstrap.css">
</head>
<body class="bg-primary">
	<h1 class="text-center text-white my-5">
		Welcome New User
	</h1>
	<div class="col-lg-4 offset-lg-4">
		<form class="bg-light p-4" action="controllers/process.php" method="POST">
			<div class="form-group">
				<label for="userName">Name</label>
				<input type="text" name="userName" class="form-control">
			</div>
			<div class="form-group">
				<label for="month">Birth Month</label>
				<input type="number" name="birthMonth" class="form-control" min="1" max="12">
			</div>
			<div class="form-group">
				<label for="date">Birth Date</label>
				<input type="number" name="birthDate" class="form-control" min="1" max="12">
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-success">Check</button>
	<!-- 			<button type="submit" class="btn btn-success">Register</button> -->
			</div>

			<?php
				session_start();
				session_destroy();
				if(isset($_SESSION['errorMsg'])){
			?>
			<p><?php echo $_SESSION['errorMsg'] ?></p>
			<?php
				}
			?>
		</form>
	</div>
</body>
</html>